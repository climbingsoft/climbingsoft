package com.mikado.climbing;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mikado.climbing.competion.Participant;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ClimbingApplicationTests {
    @Autowired
    ObjectMapper mapper;

    @Test
    void contextLoads() throws JsonProcessingException {
        System.out.println(mapper.writeValueAsString(new Participant(1, "a", "b", 3)));
    }

}
