package com.mikado.climbing.competion;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

class MemoryParticipantServiceTest {

    private final ParticipantService service = new MemoryParticipantService();

    @Test
    void get_participants() {
        assertThat(service.getParticipants(), hasSize(3));
        assertThat(service.getParticipants(), hasItem(new Participant(1, "Stas Mikhaylov", "male", 40)));
    }

    @Test
    void add_participant() {
        assertThat(service.getParticipants(), hasSize(3));
        service.add(new NewParticipant("mark", "male", 5));
        assertThat(service.getParticipants(), hasSize(4));
    }

    @Test
    void getParticipantByName() {
        assertThat(service.getParticipants(), hasSize(3));
        service.add(new NewParticipant("mark", "male", 5));
        assertThat(service.getByName("mark"), equalTo(new Participant(4, "mark", "male", 5)));
    }
}