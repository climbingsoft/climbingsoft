package com.mikado.climbing.dto;

import com.mikado.climbing.enums.UserResultStatus;

import java.util.List;
public class ClimberRowResult {

    private final UserResultStatus status;
    private final Climber climber;
    private final List<RouteResult> routes;

    public ClimberRowResult(UserResultStatus status, Climber climber, List<RouteResult> routes) {
        this.status = status;
        this.climber = climber;
        this.routes = routes;
    }

    public Climber getClimber() {
        return climber;
    }

    public List<RouteResult> getRoutes() {
        return routes;
    }

    public UserResultStatus getStatus() {
        return status;
    }
}