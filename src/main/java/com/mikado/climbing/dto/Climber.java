package com.mikado.climbing.dto;

import com.mikado.climbing.enums.Gender;
public class Climber {
    private final String code;
    private final String name;
    private final Gender gender;
    private final String team;

    public Climber(String code, String name, Gender gender, String team) {
        this.code = code;
        this.name = name;
        this.gender = gender;
        this.team = team;
    }

    public String getName() {
        return name;
    }

    public Gender getGender() {
        return gender;
    }

    public String getCode() {
        return code;
    }

    public String getTeam() {
        return team;
    }
}
