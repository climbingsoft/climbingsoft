package com.mikado.climbing.dto;

import com.mikado.climbing.enums.Status;
import java.time.LocalDate;
public class CompetitionShort {

    private final int id;
    private final String name;
    private final LocalDate date;
    private final Status status;

    public CompetitionShort(int id, String name, LocalDate date, Status status) {
        this.id = id;
        this.name = name;
        this.date = date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDate() {
        return date;
    }

    public Status getStatus() {
        return status;
    }
}
