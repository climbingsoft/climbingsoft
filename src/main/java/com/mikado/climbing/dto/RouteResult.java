package com.mikado.climbing.dto;

import com.mikado.climbing.enums.Result;
public class RouteResult {
    private final int id;
    private final int attempts;
    private final Result result;
    private final boolean auto;

    public RouteResult(int id, int attempts, Result result, boolean auto) {
        this.id = id;
        this.attempts = attempts;
        this.result = result;
        this.auto = auto;
    }

    public int getId() {
        return id;
    }

    public int getAttempts() {
        return attempts;
    }

    public Result getResult() {
        return result;
    }

    public boolean isAuto() {
        return auto;
    }
}
