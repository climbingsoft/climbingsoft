package com.mikado.climbing.dto;

public class ClimberRouteResult {

    private final int totalRoutes;
    private final int totalAttempts;

    public ClimberRouteResult(int totalRoutes, int totalAttempts) {
        this.totalRoutes = totalRoutes;
        this.totalAttempts = totalAttempts;
    }

    public int getTotalRoutes() {
        return totalRoutes;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }
}
