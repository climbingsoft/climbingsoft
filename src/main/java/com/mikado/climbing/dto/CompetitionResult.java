package com.mikado.climbing.dto;

import com.mikado.climbing.dto.ClimberShortResult;
import com.mikado.climbing.dto.CompetitionShort;
import java.util.List;
public class CompetitionResult {
    private final CompetitionShort competition;
    private final List<ClimberShortResult> climbers;

    public CompetitionResult(CompetitionShort competition, List<ClimberShortResult> climbers) {
        this.competition = competition;
        this.climbers = climbers;
    }

    public CompetitionShort getCompetition() {
        return competition;
    }

    public List<ClimberShortResult> getClimbers() {
        return climbers;
    }
}
