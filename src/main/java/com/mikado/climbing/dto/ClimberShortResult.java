package com.mikado.climbing.dto;

import com.mikado.climbing.enums.Group;
public class ClimberShortResult {

    private final String climberName;
    private final int totalRoutes;
    private final int totalAttempts;
    private final Group group;

    public ClimberShortResult( String climberName, int totalRoutes, int totalAttempts, Group group) {
        this.climberName = climberName;
        this.totalRoutes = totalRoutes;
        this.totalAttempts = totalAttempts;
        this.group = group;
    }


    public String getClimberName() {
        return climberName;
    }

    public int getTotalRoutes() {
        return totalRoutes;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public Group getGroup() {
        return group;
    }
}