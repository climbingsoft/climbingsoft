package com.mikado.climbing.dto;

import com.mikado.climbing.enums.Status;
import java.time.LocalDate;
import java.util.List;

import static com.mikado.climbing.services.CompetitionService.emptyRoute;
public class Competition extends CompetitionShort {

    private final int autoOffset;
    private final List<Route> routes;

    public Competition(int id, String name, LocalDate date, List<Route> routes, Status status) {
        super(id, name, date, status);
        this.routes = routes;
        autoOffset = 2;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public int getAutoOffset() {
        return autoOffset;
    }

    public static CompetitionShort toShortCompetition(Competition c) {
        return new CompetitionShort(c.getId(), c.getName(), c.getDate(), c.getStatus());
    }

    public Route getRouteById(int id) {
        return routes.stream().filter(x -> x.getId() == id).findAny().orElseGet(() -> emptyRoute);
    }

    public CompetitionShort toCompetitionShort() {
        return new CompetitionShort(getId(), getName(), getDate(),getStatus());
    }
}
