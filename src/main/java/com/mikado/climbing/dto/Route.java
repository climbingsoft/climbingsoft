package com.mikado.climbing.dto;

import java.util.Map;
public class Route {
    private final int id;
    private final String name;
    private final int gradeIndex;

    private static final Map<Integer, String> grades = Map.of(
            0, "-",  // прозрачный
            1, "5",  // белый
            2, "6a", // желтый
            3, "6b", // зеленый
            4, "6c", // синий
            5, "7a", // красный
            6, "7b", // черный
            7, "7c", // черный
            8, "8a"  // черный
    );

    public Route(int id, String name, Integer difficulty) {
        this.id = id;
        this.name = name;
        this.gradeIndex = difficulty;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getGradeIndex() {
        return gradeIndex;
    }

    public String getGrade() {
        return grades.get(gradeIndex);
    }
}
