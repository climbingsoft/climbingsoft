package com.mikado.climbing.services;

import com.mikado.climbing.dto.*;
import com.mikado.climbing.enums.Gender;
import com.mikado.climbing.enums.Group;
import com.mikado.climbing.enums.Result;
import com.mikado.climbing.enums.Status;
import com.mikado.climbing.enums.UserResultStatus;
import com.mikado.climbing.exceptions.NotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CompetitionService {

    private static final List<Competition> competitions = new ArrayList<>();
    private static final Map<Integer, Map<String, ClimberRowResult>> competitionResults = new HashMap<>();

    private static final Competition emptyCompetition =
            new Competition(0, "", LocalDate.ofYearDay(2070, 1), List.of(), Status.ARCHIVED);

    public static final Route emptyRoute = new Route(0, "", 0);

    private static final List<Route> routesExample = List.of(
            new Route(1, "по желтым", 1),
            new Route(2, "по синим", 3),
            new Route(3, "старт с откидки", 5),
            new Route(4, "веселые подхваты", 6),
            new Route(5, "паркур", 8),
            new Route(6, "-", 2),
            new Route(7, "тяжелая", 2),
            new Route(8, "по черным", 3),
            new Route(9, "-", 4),
            new Route(10, "жесткие мизера", 3)
    );

    static {
        var competition1 = new Competition(1, "фестиваль в Атмосфере №3", LocalDate.of(2021, 5, 20), routesExample, Status.CURRENT);
        var competition2 = new Competition(2, "фестиваль в Атмосфере №1", LocalDate.of(2021, 5, 21), routesExample, Status.CURRENT);
        var competition3 = new Competition(3, "фестиваль в Атмосфере №2", LocalDate.of(2021, 5, 21), routesExample, Status.FINISHED);
        var competition4 = new Competition(4, "серьезные старты ЦСК", LocalDate.of(2021, 5, 30), routesExample, Status.ARCHIVED);
        var competition5 = new Competition(5, "Токио летний болдерфест", LocalDate.of(2021, 6, 3), routesExample, Status.ANNOUNCED);
        competitions.add(competition1);
        competitions.add(competition2);
        competitions.add(competition3);
        competitions.add(competition4);
        competitions.add(competition5);
        competitionResults.put(competition1.getId(), new HashMap<>());
        competitionResults.put(competition2.getId(), new HashMap<>());
        competitionResults.put(competition3.getId(), new HashMap<>());
        competitionResults.put(competition4.getId(), new HashMap<>());
        competitionResults.put(competition5.getId(), new HashMap<>());
    }

    public Competition getCompetition(int id) {
        return competitions.stream().filter(competition -> competition.getId() == id).findAny().orElse(emptyCompetition);
    }

    public List<CompetitionShort> getCompetitions() {
        return competitions.stream().map(Competition::toShortCompetition).collect(Collectors.toList());
    }

    public ClimberRowResult enterUserResult(int compId, ClimberRowResult userCompResult) {
        var competitionsResults = competitionResults.get(compId);
        if (competitionsResults == null) throw new NotFoundException("competition " + compId + " not found");
        competitionsResults.put(userCompResult.getClimber().getCode(), userCompResult);
        return userCompResult;
    }

    public ClimberRowResult getUserResult(int compId, String userCode) {
        var competitionsResults = competitionResults.get(compId);
        if (competitionsResults == null) throw new NotFoundException("competition " + compId + " not found");

        if (!codeCheck(compId, userCode)) {
            return new ClimberRowResult(UserResultStatus.WRONG_CODE, null, null);
        }
        if (competitionsResults.get(userCode) == null) {
            return new ClimberRowResult(UserResultStatus.NEW_USER, null, null);
        }
        var result= competitionsResults.get(userCode);
        return new ClimberRowResult(UserResultStatus.RESULT_SAVED, result.getClimber(), result.getRoutes());
    }

    public boolean codeCheck(int compId, String code) {
        return code.endsWith("0");
    }

    public CompetitionResult getCompetitionResult(int compId) {
        var competitionResult = competitionResults.get(compId);
        var resultList = competitionResult.values()
                .stream()
                .map(x -> {
                    Climber climber = x.getClimber();
                    ClimberRouteResult climberRouteResult = getClimberTotalResult(x.getRoutes());
                    Group group = getGroup(compId, competitionResult.get(climber.getCode()));
                    return new ClimberShortResult(
                            climber.getName(),
                            climberRouteResult.getTotalRoutes(),
                            climberRouteResult.getTotalAttempts(),
                            group
                    );
                })
                .collect(Collectors.toList());

        return new CompetitionResult(competitions.get(compId - 1).toCompetitionShort(), resultList);
    }

    public Competition createCompetition() {
        Competition newCompetition = new Competition(competitions.size(), "", LocalDate.now().plusMonths(1), List.of(), Status.PREPARING);
        competitions.add(newCompetition);
        competitionResults.put(newCompetition.getId(), new HashMap<>());
        return newCompetition;
    }


    ClimberRouteResult getClimberTotalResult(List<RouteResult> results) {
        int totalRoutes = (int) results.stream().filter(x -> x.isAuto() || x.getResult() == Result.DONE).count();
        int totalAttempts =
                results.stream()
                        .map(x -> {
                            var attempts = 0;
                            if (x.isAuto()) {
                                attempts++;
                            } else if (x.getResult() == Result.DONE) {
                                attempts = +x.getAttempts();
                            }
                            return attempts;
                        })
                        .mapToInt(Integer::intValue)
                        .sum();

        return new ClimberRouteResult(totalRoutes, totalAttempts);
    }

    private Group getGroup(int compId, ClimberRowResult climberRowResult) {
        int maxRouteGrade = climberRowResult.getRoutes().stream()
                .map(x -> competitions.get(compId).getRouteById(x.getId()).getGradeIndex())
                .reduce(Integer::max)
                .orElse(0);
        Gender gender = climberRowResult.getClimber().getGender();
        if (maxRouteGrade >= 5) {
            return (gender == Gender.MALE) ? Group.SPORT_MALE : Group.SPORT_FEMALE;
        }
        if (maxRouteGrade == 4) {
            return (gender == Gender.MALE) ? Group.AMATEUR_MALE : Group.AMATEUR_FEMALE;
        }
        return (gender == Gender.MALE) ? Group.LIGHT_MALE : Group.LIGHT_FEMALE;
    }

}
