package com.mikado.climbing.controller;

import com.mikado.climbing.competion.NewParticipant;
import com.mikado.climbing.competion.Participant;
import com.mikado.climbing.competion.ParticipantService;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/participants")
@CrossOrigin(origins = "http://localhost:3000")
public class ParticipantsController {

    final ParticipantService participantService;

    public ParticipantsController(ParticipantService participantService) {
        this.participantService = participantService;
    }

    @GetMapping
    public List<Participant> list() {
        return participantService.getParticipants();
    }

    @GetMapping("{id}")
    public Participant getParticipant(@PathVariable int id) {
        return participantService.getById(id);
    }

    @PostMapping()
    public Participant addParticipant(@RequestBody Map<String, String> p) {
        participantService.add(new NewParticipant(p.get("name"), p.get("gender"), Integer.valueOf(p.get("age"))));
        return null;
    }

}
