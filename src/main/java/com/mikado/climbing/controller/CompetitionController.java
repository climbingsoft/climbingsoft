package com.mikado.climbing.controller;

import com.mikado.climbing.dto.ClimberRowResult;
import com.mikado.climbing.dto.Competition;
import com.mikado.climbing.dto.CompetitionResult;
import com.mikado.climbing.dto.CompetitionShort;
import com.mikado.climbing.services.CompetitionService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/competitions")
@CrossOrigin(origins = "http://localhost:3000")
public class CompetitionController {

    final CompetitionService competitionService;

    public CompetitionController(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }

    @GetMapping
    public List<CompetitionShort> getCompetitions() {
        return competitionService.getCompetitions();
    }

    @PostMapping()
    public Competition createCompetition() {
        return competitionService.createCompetition();
    }

    @GetMapping("{compId}")
    public Competition getCompetition(@PathVariable int compId) {
        return competitionService.getCompetition(compId);
    }

    @GetMapping("{compId}/result")
    public CompetitionResult getCompetitionResult(@PathVariable int compId) {
        return competitionService.getCompetitionResult(compId);
    }

    @PutMapping("{compId}/user")
    public ClimberRowResult enterClimberResult(@PathVariable int compId, @RequestBody ClimberRowResult climberRowResult) {
        return competitionService.enterUserResult(compId, climberRowResult);
    }

    @GetMapping("{compId}/user/{climberCode}")
    public ClimberRowResult getClimberResult(@PathVariable int compId, @PathVariable String climberCode) {
        return competitionService.getUserResult(compId, climberCode);
    }
}
