package com.mikado.climbing.enums;

public enum Result {
    UNTOUCHED,
    DONE,
    NONE
}
