package com.mikado.climbing.enums;

public enum UserResultStatus {
    WRONG_CODE,
    NEW_USER,
    RESULT_SAVED
}
