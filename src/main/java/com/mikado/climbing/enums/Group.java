package com.mikado.climbing.enums;

public enum Group {
    SPORT_MALE,
    SPORT_FEMALE,
    AMATEUR_MALE,
    AMATEUR_FEMALE,
    LIGHT_MALE,
    LIGHT_FEMALE
}
