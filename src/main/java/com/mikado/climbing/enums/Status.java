package com.mikado.climbing.enums;

public enum Status {
    PREPARING,
    ANNOUNCED,
    CURRENT,
    FINISHED,
    ARCHIVED
}
