package com.mikado.climbing.enums;

public enum Gender {
    MALE,
    FEMALE
}
