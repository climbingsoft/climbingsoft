package com.mikado.climbing.competion;

import com.mikado.climbing.exceptions.NotFoundException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class MemoryParticipantService implements ParticipantService {
    private int id = 4;

    private List<Participant> participants = new ArrayList<>() {{
        add(new Participant(1, "Stas Mikhaylov", "male", 40));
        add(new Participant(2, "Dianka Mikhaylova", "female", 30));
        add(new Participant(3, "Ilya Mikhaylov", "male", 20));
    }};

    @Override
    public List<Participant> getParticipants() {
        return participants;
    }

    @Override
    public Participant getById(int id) {
        return null;
    }

    @Override
    public void add(NewParticipant p) {
        participants.add(new Participant(id, p));
        id++;
    }

    @Override
    public Participant getByName(String name) {
        return participants.stream()
                .filter(participant -> name.equals(participant.getName()))
                .findAny()
                .orElseThrow(() -> new NotFoundException("participant " + name + "not found"));
    }
}
