package com.mikado.climbing.competion;

import java.util.List;
public interface ParticipantService {

    List<Participant> getParticipants();

    Participant getById(int id);

    void add(NewParticipant p);

    Participant getByName(String name);
}
