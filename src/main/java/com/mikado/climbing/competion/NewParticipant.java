package com.mikado.climbing.competion;

public class NewParticipant {
    String name;
    String gender;
    Integer age;

    public NewParticipant(String name, String gender, Integer age) {
        this.name = name;
        this.gender = gender;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public Integer getAge() {
        return age;
    }
}
