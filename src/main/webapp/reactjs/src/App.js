import React from "react";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {Container} from "react-bootstrap";
import ParticipantsList from "./components/ParticipantsList";
import Welcome from "./components/Welcome";
import AddResults from "./components/AddResults";
import CompetitionPage from "./components/CompetitionPage";
import ShowResults from "./components/ShowResults";
import Header from "./components/Header";
import NewRulesText from "./components/NewRulesText";

class App extends React.Component{
  constructor(props) {
    super(props);
    this.state = {width: 500};
  }

  componentDidMount() {
    this.setState({width: window.innerWidth});
  }

  getContent = () => {
    return (
        <Switch>
          <Route path="/" exact component={Welcome}/>
          <Route path="/participants" exact component={ParticipantsList}/>
          <Route path="/competitions/:compId/add_results" exact component={AddResults}/>
          <Route path="/competitions/:compId/show_results" exact component={ShowResults}/>
          <Route path="/competitions/:compId" exact component={CompetitionPage}/>
          <Route path="/new_rules" exact component={NewRulesText}/>
        </Switch>
    )
  }

  render() {
    return (
      <Router>
        <div className="App">
          {this.state.width < 500 ? this.getContent() :
          <Container>
              <Route path="/" component={Header}/>
              {this.getContent()}
          </Container>}
        </div>
      </Router>
    );
  }

}

export default App;
