import React from "react";
import {Button, Card, Form} from "react-bootstrap";
import Service from "../Service";

export default class ParticipantForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = this.initialState;
    }

    initialState = {
        name: "",
        gender: "male",
        age: ""
    }

    submitParticipant = (e) => {
        e.preventDefault();

        if (isNaN(this.state.age)) {
            return;
        }


        new Service().postParticipant({name: this.state.name, gender: this.state.gender, age: this.state.age});
        this.setState({...this.initialState});
        this.props.onSubmitBtn();
    }

    participantChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    render() {
        return (
            <Form onSubmit={this.submitParticipant}>
                <Card>
                    <Card.Body>
                        <Form.Group controlId="formGridName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                required
                                type="text" name="name"
                                value={this.state.name}
                                onChange={this.participantChange}
                                placeholder="Enter name"
                            />
                        </Form.Group>
                        <Form.Group controlId="formGridGender">
                            <Form.Label>Gender</Form.Label>
                            <Form.Control as="select" defaultValue="Choose..."
                                          value={this.state.gender}
                                          onChange={this.participantChange}
                                          name="gender">
                                <option value="male">male</option>
                                <option value="female">female</option>
                            </Form.Control>
                        </Form.Group>
                        <Form.Group controlId="formGridAge">
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                                required
                                type="number" name="age"
                                value={this.state.age}
                                onChange={this.participantChange}
                                placeholder="Enter age"
                            />
                        </Form.Group>
                    </Card.Body>
                    <Card.Footer>
                        <Button size="sm" variant="success" type="submit">
                            Submit
                        </Button>{' '}
                        <Button size="sm" variant="info" type="button" onClick={this.props.onClose}>
                            Close
                        </Button>
                    </Card.Footer>
                </Card>
            </Form>
        )
    }
}