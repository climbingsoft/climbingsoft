import React from "react";

import {Navbar, Nav} from "react-bootstrap";
import {Link} from "react-router-dom";

class NavigationBar extends React.Component {
    render() {
        return (
            <div>
                <Navbar variant="light" bg="light">
                    <Link to="" className="navbar-brand">
                        Main page
                    </Link>
                    <Nav className="mr-auto">
                        <Link to="participants" className="nav-link">Participants list</Link>
                        <Link to="add_results" className="nav-link">Add results</Link>
                    </Nav>
                </Navbar>
            </div>
        )
    }
}

export default NavigationBar;