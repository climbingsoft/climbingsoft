import React from "react";
import {Button, Card, Modal, Table} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faUsers} from "@fortawesome/free-solid-svg-icons";
import Service from "../Service";
import ParticipantForm from "./ParticipantForm";

export default class ParticipantsList extends React.Component {
    constructor(props) {
        super(props);
        this.service = new Service();
        this.state = {
            participants: [],
            showModal: false
        };
    }

    handleClose = () => this.setState({showModal: false});
    handleShow = () => this.setState({showModal: true});

    componentDidMount() {
        this.updateList();
    }

    updateList = () => {
        this.service.getAllParticipants()
            .then(participants => this.setState({participants}))
            .catch(() => console.log("Something goes wrong"));
    }

    render() {

        const {participants} = this.state;

        return (
            <div>
                <Card className="border">
                    <Card.Header><FontAwesomeIcon icon={faUsers} /> Participants List</Card.Header>
                    <Card.Body>
                        <Table bordered hover striped>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                </tr>
                            </thead>
                            <tbody>
                            {participants.length === 0 ?
                                <tr align='center'>
                                    <td colSpan='6'>No Participants available</td>
                                </tr> :
                                participants.map((pers, index) => (
                                    <tr key={index}>
                                        <td>{pers.name}</td>
                                        <td>{pers.gender}</td>
                                        <td>{pers.age}</td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </Table>
                    </Card.Body>
                    <Card.Footer>
                        <Button variant="primary" style={{float: "right"}} onClick={this.handleShow}>Add participant</Button>
                    </Card.Footer>
                </Card>

                <Modal show={this.state.showModal} onHide={this.handleClose}
                       size="lg"
                       aria-labelledby="contained-modal-title-vcenter"
                       centered>
                    <Modal.Header closeButton>
                        <Modal.Title>Creating a participant</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ParticipantForm onClose={this.handleClose} onSubmitBtn={this.updateList}/>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}