import React from "react";
import {Button, Card} from "react-bootstrap";
import Service from "../Service";

export default class CompetitionPage extends React.Component {
    constructor(props) {
        super(props);
        this.service = new Service();
        this.state = {

        }
    }

    componentDidMount() {
        this.service.getCompetitionById(this.props.match.params.compId)
            .then((data) => {
                this.setState({
                    id: data.id,
                    date: data.date,
                    name: data.name,
                    status: data.status
                })
            })
    }

    render() {
        const {name, status} = this.state;
        return (
            <Card>
                <Card.Header as="h2" className={"text-center"}>{name}</Card.Header>
                <Card.Body>
                    <Button variant="outline-success" style={{width: "100%", marginBottom: "10px",
                        display: ((status === "ARCHIVED" || status === "FINISHED" || status === "CURRENT") ? "block" : "none")}}
                            onClick={() => this.props.history.push(`${this.props.match.params.compId}/show_results`)}>
                        Итоговые результаты
                    </Button>
                    <Button variant="outline-success" style={{width: "100%",
                        display: ((status === "CURRENT") ? "block" : "none")}}
                            onClick={() => this.props.history.push(`${this.props.match.params.compId}/add_results`)}>
                        Ввести результат
                    </Button>
                    <p style={{marginTop: "20px"}}>
                        Some text.
                        More text.
                    </p>
                </Card.Body>
            </Card>
        )
    }
}