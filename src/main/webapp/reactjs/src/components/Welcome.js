import React from "react";
import {Card, Table} from "react-bootstrap";
import Service from "../Service";

class Welcome extends React.Component {

    constructor(props) {
        super(props);
        this.service = new Service();
        this.state = {
            competitions: []
        }
    }

    componentDidMount() {
        this.service.getAllCompetitions()
            .then((data) => this.setState({competitions: data}))
            .catch(() => console.log("Something goes wrong"));
    }

    getDate = (str) => {
        return new Date(str).toLocaleString('ru', {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        }).slice(0, -2);
    }

    statuses = {
        CURRENT: "идут",
        FINISHED: "закончились",
        ARCHIVED: "архив",
        ANNOUNCED: "скоро"
    }

    render() {
        return (
            <Card>
                <Card.Header as={"h2"} className={"text-center"}>
                    Список соревнований
                </Card.Header>
                <Card.Body>
                    {this.state.competitions.length === 0 ? <p>No competitions available</p> :
                        <>
                        {this.state.competitions.filter(comp => comp.status === "CURRENT").length > 0 ?
                                <>
                                    <Table hover>
                                        <thead>
                                        Соревнования идут
                                        </thead>
                                        <tbody>
                                            {this.state.competitions.filter(comp => comp.status === "CURRENT").map((comp) => {
                                                return (
                                                    <tr key={"Current" + comp.id} onClick={() => this.props.history.push(`competitions/${comp.id}`)}>
                                                        <td width="25%">{this.getDate(comp.date)}</td>
                                                        <td width="50%">{comp.name}</td>
                                                        <td>{this.statuses[comp.status]}</td>
                                                    </tr>
                                                )
                                            })}
                                        </tbody>
                                    </Table>
                                </> : null}
                            <Table hover style={{marginTop: "40px"}}>
                                <thead style={{marginTop: "100px"}}>
                                    Все соревнования
                                </thead>
                                <tbody>
                                {this.state.competitions.map((comp) => {
                                    return (
                                        <tr key={comp.id} onClick={() => this.props.history.push(`competitions/${comp.id}`)}>
                                            <td width="25%">{this.getDate(comp.date)}</td>
                                            <td width="50%">{comp.name}</td>
                                            <td>{this.statuses[comp.status]}</td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </>
                    }
                </Card.Body>
            </Card>
        );
    }
}

export default Welcome;