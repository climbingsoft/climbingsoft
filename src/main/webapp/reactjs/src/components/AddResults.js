import React from "react";
import {Card, Form, Button, Table, Modal, ListGroup} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {
    faAngleLeft,
    faAngleRight,
    faCheck,
    faEllipsisH,
    faQuestion,
    faTimes,
    faTrash
} from "@fortawesome/free-solid-svg-icons";
import Service from "../Service";

import '../styles.css';


export default class AddResults extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            gender: "MALE",
            code: "",
            team: "",
            tracks: [],
            compName: "",
            maxTrack: 0,
            submitText: "",
            compId: "",
            enterStatus: "No code",
            users: [
                // {code: 3456, name: "Ilya Mikhaylov"},
                // {code: 34346, name: "Stas Mikhaylov"},
                // {code: 1256, name: "Diana Mikhaylova"}
            ],
            showModal: false
        }
        this.service = new Service();
    }

    colors = [
        "fff",
        "fff", //white
        "feef9f", //yellow
        "d0fe9f", //green
        "c3d6fe", //blue
        "ffab96", //red
        "959595", //black
        "959595",
        "959595"

    ]


    componentDidMount() {

        this.setState({compId : this.props.match.params.compId});

        //localStorage.setItem(`lastUser${this.state.compId}`, "123");
        //localStorage.clear();
        this.service.getCompetitionById(this.props.match.params.compId)
            .then(comp => {
                comp.routes.forEach((route) => {
                    route.result = "UNTOUCHED";
                    route.try = 0;
                    route.auto = false;
                })
                this.setState({
                    tracks: comp.routes,
                    compName: comp.name,
                    compDate: comp.date,
                    status: comp.status,
                    autoOffSet: comp.autoOffset,
                    id: comp.id
                });
            })
            .catch(() => console.log("AAAAAAA"));


        const lastUser = localStorage.getItem(`lastUser${this.props.match.params.compId}`);
        if (lastUser) {
            this.loadUserData(JSON.parse(lastUser));
        }


        const users = localStorage.getItem(`users${this.state.compId}`);
        if (users) {
            this.setState({users: JSON.parse(users)});
        }


    }

    loadUserData = (code) => {
        code = Number.parseInt(code);
        this.setState({code});
        this.service.getUserByCode(code, this.props.match.params.compId)
            .then(res => {
                if (res.status !== "RESULT_SAVED") {
                    console.log("Something wrong with loadUserData, the status is " + res.status)
                    return;
                }
                console.log(res);
                const user = res.climber;
                this.setState({
                    name: user.name,
                    gender: user.gender,
                    team: user.team,
                    enterStatus: "Entered"
                })
                localStorage.setItem(`lastUser${this.state.compId}`, code);
                this.addLocalUser({code: Number.parseInt(code), name: user.name});
            })
    }

    updateAutoResults = async () => {
        let maxGrade = 0;
        this.state.tracks.forEach((track) => {
            if (track.result === "DONE" && track.gradeIndex >= maxGrade) {
                maxGrade = track.gradeIndex;
            }
        })

        const newTracks = this.state.tracks.map((track) => {
            return ({...track, auto: (track.gradeIndex <= maxGrade - this.state.autoOffSet)})
        })
        await this.setState({tracks: newTracks});
    }


    submitResults = async (e) => {
        e.preventDefault();
        const {code, name, gender, team, tracks} = this.state;

        const result = {
            status: "OLD_USER",
            climber: {
                code,
                name,
                gender,
                team
            },
            routes: tracks.map((track) => {
                return {
                    attempts: track.try,
                    auto: track.auto,
                    id: track.id,
                    result: track.result
                }
            })
        }
        const res = await this.service.putParticipantResults(result, this.props.match.params.compId);
        if (res.ok) {
            this.setState({submitText: "Данные успешно отправленны"})
        } else {
            this.setState({submitText: "Что-то пошло не так"})
        }
        setTimeout(() => {this.setState({submitText: ""})}, 5000);
    }


    resultsChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        });
    }

    changeTrack = (ind, newTrack) => {
        this.setState((state) => {
            const arr1 = state.tracks.slice(0, ind);
            const arr2 = state.tracks.slice(ind + 1);
            const track = state.tracks[ind];
            return {tracks: [...arr1, {...track, ...newTrack}, ...arr2]};
        })
    }

    onResultButton = async (ind, track) => {
        let res = "UNTOUCHED";
        let tr = track.try;
        if (track.result === "UNTOUCHED") {
            res = "DONE";
            tr = (tr === 0 ? 1 : tr);
        } else if (track.result === "DONE") {
            res = "NONE";
        }
        const newTrack = {
            result: res,
            try: tr
        }
        await this.changeTrack(ind, newTrack);
        await this.updateAutoResults();
    }

    getButton = (ind, track) => {
        if (track.auto) {
            return (
                <Button variant={"success"} style={{fontSize: "16px", fontWeight: "bolder"}}
                        disabled>
                    A
                </Button>
            )
        }
        let variant = "light";
        let icon = faQuestion;
        let style = {};
        if (track.result === "DONE") {
            variant = "success";
            icon = faCheck;
        } else if (track.result === "NONE") {
            variant = "danger";
            icon = faTimes;
        } else {
            style = {border: "2px solid grey", color: "grey"};
        }
        return (
            <Button variant={variant} style={style}
                    onClick={() => this.onResultButton(ind, track)}>
                <FontAwesomeIcon icon={icon}/>
            </Button>
        )
    }

    render() {
        return (
            <div>
                {this.myVerticallyCenteredModal()}
                {this.state.enterStatus === "No code" ? this.enterCode() : null}
                {this.state.enterStatus === "No name" ? this.enterName() : null}
                <Card className="border" style={{display: (this.state.enterStatus === "Entered" ? "block" : "none")}}>
                    <Card.Header as={"h2"} className={"text-center clickable"}
                                 onClick={() => this.props.history.push(`/competitions/${this.props.match.params.compId}`)}
                                 >
                        {this.state.compName}
                    </Card.Header>
                    <Form onSubmit={this.submitResults} id="resultsFormId">
                        <Card.Body style={{padding: "0px"}}>
                            <div style={{float: "right", marginTop: "10px", verticalAlign: "middle", height: "40px", width: "100%"}}>
                                <h3 style={{position: "absolute", right: "60px"}}>{this.state.code}{" "}{this.state.name}</h3>
                                <Button variant={"outline-secondary"} size="sm" style={{position: "absolute", right: "20px"}}
                                    onClick={() => this.setState({showModal: true})}>
                                    <FontAwesomeIcon icon={faEllipsisH}/>
                                </Button>
                            </div>
                            {(this.state.tracks.length > 0) ?
                                <Table bordered className="text-center">
                                    <tbody>
                                        <tr>
                                            <th>Route</th>
                                            <th>Result</th>
                                            <th>Attempts</th>
                                        </tr>
                                        {this.state.tracks.map((track, ind) => {
                                            return (
                                                <tr key={ind} style={{backgroundColor: '#' + this.colors[track.gradeIndex]}}>
                                                    <td>
                                                        <div style={{float: "left", marginTop: "8px"}}>
                                                            {ind + 1}{" "}{track.name}
                                                        </div>
                                                    </td>
                                                    <td>
                                                        {this.getButton(ind, track)}
                                                    </td>
                                                    <td style={{fontSize: "20px", paddingLeft: "0px", paddingRight: "0px"}}>
                                                        <Button style={{marginRight: "0px", marginLeft: "0", border: "2px solid grey"}} variant="light"
                                                                onClick={() => this.changeTrack(ind, {...track, try: track.try - 1})}
                                                                disabled={(track.result === "DONE" && track.try < 2) || track.try < 1 || track.auto}><FontAwesomeIcon icon={faAngleLeft}/></Button>
                                                          <div style={{display: "inline-block", width: "30px"}}>{(track.auto ? 1 : track.try)}</div>
                                                        <Button style={{marginLeft: "0px", marginRight: "0", border: "2px solid grey"}} variant="light"
                                                                onClick={() => this.changeTrack(ind, {...track, try: track.try + 1})}
                                                                disabled={track.try > 98 || track.auto}><FontAwesomeIcon icon={faAngleRight}/></Button>
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </Table> : <p>Can not find routes</p>}
                        </Card.Body>
                        <Card.Footer style={{"textAlign":"right"}}>
                            {this.state.submitText}{" "}
                            <Button size="sm" variant="success" type="submit" disabled={this.state.submitText !== ""}>
                                Submit
                            </Button>
                        </Card.Footer>
                    </Form>
                </Card>
            </div>
        )
    }

    submitCode = async (e) => {
        e.preventDefault();

        const {code} = this.state;

        const res = await this.service.getUserByCode(code, this.state.compId);
        console.log(res);
        if (res.status === "WRONG_CODE") {
            console.log("The code is not connect");
        } else if (res.status === "NEW_USER") {
            this.setState({enterStatus: "No name"})
        } else {
            this.setState({enterStatus: "Entered"});
            this.loadUserData(code);
        }
    }

    submitName = async (e) => {
        e.preventDefault();

        const {code, name, gender, team} = this.state;

        const result = {
            climber: {
                code,
                name,
                gender,
                team
            },
            routes: []
        }

        await this.service.putParticipantResults(result, this.state.compId);
        await this.setState({enterStatus: "Entered"});
        await this.loadUserData(code);
    }

    enterCode = () => {
        return (
            <Card className="border">
                <Card.Header as={"h2"} className={"text-center clickable"} onClick={() => this.props.history.push(`/competitions/${this.props.match.params.compId}`)}>
                    {this.state.compName}
                </Card.Header>
                <Form onSubmit={this.submitCode} id="resultsFormId">
                    <Card.Body style={{padding: "0px"}}>
                        <div style={{padding: "20px"}}>
                            <Form.Group controlId="formCode">
                                <Form.Label>
                                    Код участника
                                </Form.Label>
                                <Form.Control
                                    required
                                    type="text" name="code"
                                    value={this.state.code}
                                    onChange={this.resultsChange}
                                    placeholder=""
                                    style={{width: "50%"}}
                                />
                            </Form.Group>
                        </div>
                    </Card.Body>
                    <Card.Footer style={{"textAlign":"right"}}>
                        {this.state.submitText}{" "}
                        <Button size="sm" variant="success" type="submit">
                            Далее
                        </Button>
                    </Card.Footer>
                </Form>
            </Card>
        )
    }

    enterName = () => {
        return (
            <Card className="border">
                <Card.Header as={"h2"} className={"text-center clickable"} onClick={() => this.props.history.push(`/competitions/${this.props.match.params.compId}`)}>
                    {this.state.compName}
                </Card.Header>
                <Form onSubmit={this.submitName} id="resultsFormId">
                    <Card.Body style={{padding: "0px"}}>
                        <div style={{padding: "20px"}}>
                            <Form.Group controlId="formCode">
                                <Form.Label>
                                    Код участника
                                </Form.Label>
                                <Form.Control
                                    readOnly
                                    type="text" name="code"
                                    value={this.state.code}
                                    onChange={this.resultsChange}
                                    placeholder=""
                                    style={{width: "50%"}}
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>
                                    Участник
                                </Form.Label>
                                <Form.Control
                                    required
                                    type="text" name="name"
                                    value={this.state.name}
                                    onChange={this.resultsChange}
                                    placeholder="введите ФИО"
                                />
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>
                                    Пол
                                </Form.Label>
                                <Form.Control
                                    required
                                    as="select"
                                    type="text" name="gender"
                                    value={this.state.gender}
                                    onChange={this.resultsChange}
                                    style={{width: "50%"}}>
                                    <option value="MALE">мужчина</option>
                                    <option value="FEMALE">женщина</option>
                                </Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>
                                    Команда
                                </Form.Label>
                                <Form.Control
                                    type="text" name="team"
                                    value={this.state.team}
                                    onChange={this.resultsChange}
                                    placeholder=""
                                />
                            </Form.Group>
                        </div>
                    </Card.Body>
                    <Card.Footer style={{"textAlign":"right"}}>
                        {this.state.submitText}{" "}
                        <Button size="sm" variant="success" type="submit">
                            Далее
                        </Button>
                    </Card.Footer>
                </Form>
            </Card>
        )
    }

    myVerticallyCenteredModal = () => {
        return (
            <Modal
                show={this.state.showModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                onHide={() => this.setState({showModal: false})}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Сменить участника
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ListGroup>
                        {this.state.users.map((user) => {
                            return (
                                <ListGroup.Item name={"change"} action key={user.code} onClick={(e) => {
                                    this.onChangeUserButton(e, user.code);
                                }}>
                                    {user.code}{" "}{user.name}
                                    <FontAwesomeIcon icon={faTrash} style={{float: "right"}} onClick={() => this.removeLocalUser(user.code)}/>
                                </ListGroup.Item>
                            )
                        })}
                        <ListGroup.Item action onClick={() => {
                            this.setState({enterStatus: "No code", showModal: false});
                        }}>
                            Новый участник
                        </ListGroup.Item>
                    </ListGroup>
                </Modal.Body>
            </Modal>
        );
    }

    onChangeUserButton = (e, code) => {
        if (e.target.name === "change") {
            this.loadUserData(code);
            this.setState({showModal: false});
        } else {

        }
    }

    addLocalUser = async (user) => {
        const users = JSON.parse(localStorage.getItem(`users${this.state.compId}`));
        if (!users) {
            await localStorage.setItem(`users${this.state.compId}`, JSON.stringify([user]));
            await this.setState({users: [user]})
            return;
        }
        if (await users.filter((us) => us.code === user.code).length > 0) {
            return;
        }
        await users.push(user);
        await this.setState({users});
        await localStorage.setItem(`users${this.state.compId}`, JSON.stringify(users));
    }

    removeLocalUser = async (code) => {
        if (code == this.state.code) {
            localStorage.removeItem(`lastUser${this.state.compId}`);
            this.setState({enterStatus: "No code"});
            this.setState({showModal: false});
        }
        const users = JSON.parse(localStorage.getItem(`users${this.state.compId}`));
        await this.setState({users: users.filter((us) => us.code != code)});
        await localStorage.setItem(`users${this.state.compId}`, JSON.stringify(users.filter((us) => us.code !== code)));
    }
}