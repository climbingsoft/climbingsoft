import React from "react";
import {Link} from "react-router-dom";

import '../styles.css';

export default class Header extends React.Component {

    toMainPage = () => {
        this.props.history.push(``)
    }

    render() {
        return (
            <div style={{backgroundColor: "#55708e", marginBottom: "10px", marginTop: "10px", width: "100%", height: "80px", position: "relative", color: "white"}}>
                <img className={'clickable'} alt="" src={"/img.png"} style={{width: "80px", height: "80px", marginRight: "10px", position: "absolute", left: "0px", top: "0px"}} onClick={this.toMainPage}/>
                <div style={{fontSize: "20px", marginTop: "10px", marginLeft: "90px", display: "inline-block"}}>
                    Скалолазные соревнования
                    <div> - считаем <Link to="/new_rules" style={{color: "white", textDecoration: "underline"}}>правильно</Link></div>
                </div>
            </div>
        )
    }
}