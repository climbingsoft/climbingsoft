import React from "react";
import {Button, ButtonGroup, Card, Table} from "react-bootstrap";
import Service from "../Service";
import '../styles.css';

export default class ShowResults extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            climbers: [],
            groupFilter: false,
            name: "",
            id: 0
        }
    }

    groups = [
        'SPORT_MALE',
        'LIGHT_MALE',
        'SPORT_FEMALE',
        'LIGHT_FEMALE'
    ]

    componentDidMount() {
        this.setState({id: this.props.match.params.compId})
        new Service().getCompetitionResults(this.props.match.params.compId)
            .then(data => {
                this.setState({climbers: data.climbers.sort((a, b) => {
                        if (a.totalRoutes === b.totalRoutes) {
                            return a.totalAttempts - b.totalAttempts;
                        } else {
                            return b.totalRoutes - a.totalRoutes;
                        }
                    })});
                this.setState({name: data.competition.name});
            })
    }

    render() {
        return (
            <Card>
                <Card.Header as={"h2"} className={"text-center clickable"} onClick={() => this.props.history.push(`/competitions/${this.state.id}`)}>
                    {this.state.name}
                </Card.Header>
                <Card.Body>
                    <div style={{textAlign: "center", padding: "5px"}}>
                        <ButtonGroup>
                            <Button variant={(!this.state.groupFilter ? "primary" : "outline-primary")}
                                    checked={!this.state.groupFilter}
                                    onClick={() => this.setState({groupFilter: false})}
                                    style={{marginLeft: "5px"}}
                            >
                                Все вместе
                            </Button>
                            <Button variant={(this.state.groupFilter ? "primary" : "outline-primary")}
                                    checked={this.state.groupFilter}
                                    onClick={() => this.setState({groupFilter: true})}
                            >
                                По группам
                            </Button>
                        </ButtonGroup>
                    </div>
                    <Table>
                        <thead>
                            <tr>
                                <td>Место</td>
                                <td>Имя</td>
                                <td>Результат</td>
                                <td>Команда</td>
                                <td>Пол</td>
                                <td>Группа</td>
                            </tr>
                        </thead>
                        {(!this.state.groupFilter ? <tbody>
                            {this.state.climbers.map((pers, id) => {
                                return (
                                    <tr key={id}>
                                        <td>{id + 1}</td>
                                        <td>{pers.climberName}</td>
                                        <td>{pers.totalRoutes + " / " + pers.totalAttempts}</td>
                                        <td>{pers.group}</td>
                                    </tr>
                                )})
                            }
                        </tbody> : <tbody>
                            {this.groups.map(group => {
                                return (
                                    <>
                                        <tr key={group}>
                                            <td colSpan={6}>
                                                {group}
                                            </td>
                                        </tr>
                                        {this.state.climbers.filter(climber => climber.group === group).map((climber, ind) => {
                                            return (<tr key={group + ind}>
                                                <td>{ind + 1}</td>
                                                <td>{climber.climberName}</td>
                                                <td>{climber.totalRoutes + "/" + climber.totalAttempts}</td>
                                                <td>{climber.group}</td>
                                            </tr>)
                                        })}
                                    </>
                                )
                            })}
                        </tbody>)}
                    </Table>
                </Card.Body>
            </Card>
        )
    }
}