export default class Service {
    constructor() {
        this._apiBase = `${window.location.href.slice(0, -window.location.pathname.length)}/api`;

        if (this._apiBase === "http://localhost:3000/api") {
            this._apiBase = "http://localhost:8080/api";
        }

        this.headers = new Headers();

    }

    async getRecource(url) {
        const res = await fetch(`${this._apiBase}${url}`);

        if (!res.ok) {
            throw new Error('AAAAAAA');
        }

        return await res.json();
    }

    async getAllParticipants() {
        const res = await this.getRecource('/participants');
        return res;
    }

    async getParticipantById(id) {
        const res = await this.getRecource(`/participants/${id}`);
        return res;
    }

    async postParticipant(participant) {
        const res = await fetch(`${this._apiBase}/participants`, {
            method: 'POST',
            body: JSON.stringify(participant),
            headers: {
                "Content-Type" : "application/json"
            }
        });

        if (!res.ok) {
            throw new Error("AAAAA");
        }

        return await res.json();
    }

    async putParticipantResults(result, compId) {
        const res = await fetch(`${this._apiBase}/competitions/${compId}/user`, {
            method: 'PUT',
            body: JSON.stringify(result),
            headers: {
                "Content-Type":"application/json"
            }
        });

        return res;
    }

    async getAllCompetitions() {
        const res = await this.getRecource('/competitions');
        return res;
    }

    async getCompetitionById(id) {
        const res = await this.getRecource(`/competitions/${id}`);
        return res;
    }

    async getCompetitionResults(id) {
        return await this.getRecource(`/competitions/${id}/result`)
    }

    async getUserByCode(code, compId) {
        return await this.getRecource(`/competitions/${compId}/user/${code}`)
    }


}