#!/bin/bash

cd src/main/webapp/reactjs
npm i
npm run build
rm -rf ../../resources/static/*
cp -a build/. ../../resources/static
cd ../../../..

