## сlimbresult

Программа расчета протоколов скалолазных фестивалей с вычислением результатов как на кубках мира (пролазы/попытки)

### Swagger
for local usage:
- http://localhost:8080/swagger-ui.html
- http://localhost:8080/v2/api-docs

### Build
- Перейти в папку проекта
- ./react_build.sh
- ./gradlew clean build   

### create a docker and push to Amazon ECR (after Build)
- docker build -t climbing:"version" .
- docker tag climbing:"version" 514535933273.dkr.ecr.eu-central-1.amazonaws.com/climbingsoft:"version"
- docker push 514535933273.dkr.ecr.eu-central-1.amazonaws.com/climbingsoft:"version"

### Local launch (after Build)
- ./gradlew bootRun 
- http://localhost:8080/

### Launch on Amazon EC2
ssh -i awsclimb.pem ec2-user@3.67.42.234



