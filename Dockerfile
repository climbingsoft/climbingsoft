FROM openjdk:11-jdk-slim
WORKDIR /app
COPY build/libs/climbing-1.jar climbing.jar
RUN mkdir /tmp/tomcat static

ENTRYPOINT java $JAVA_OPTIONS -jar climbing.jar
